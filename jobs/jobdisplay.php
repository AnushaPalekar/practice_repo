<html>
<html>
<head>
	<title> Naukari for her-Form </title>
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="custom-style.css"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<header>
        <div class="container">
            <div class="header">
			  <div class="col-md-12">	
              <h1> List Of Vacancies </h1>
              </div>
            </div>
        </div>
    </header>
	<!--***********************************************************************************-->
		
	<div class="conatainer">
		<div class="col-lg-1">
        </div>
        <div class="col-lg-10 col-sm-1">
				<div class="panel panel-default">
                        <div class="panel-heading">
                           <h3> Job Vacancies</h3>
                        </div>
                        <div class="panel-body">
							<form>
								<table class="table table-striped" border="2" style ="cellspacing :20px cellpading: 10px">
									<tr>
									<th>Company Name</th>
									<th>JobTitle</th>
									<th>Location</th>
									<th>Education</th>
									<th>Experience</th>
									<th>Salary</th>
									<th>Description</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Edit</th>
									<th>Delete</th>
									</tr>
													<?php

														$conn=mysqli_connect("localhost","root")
														or die("Cant Connect to server-".mysqli_errno()." : ".mysqli_error());
														
														//echo "Connected server successfully";
														
														mysqli_select_db($conn,"phpassignment") or die("Cant Connect to Database - ".mysqli_errno()." : ".mysqli_error());
														
														//echo "Connected db successfully"; 
														
														$query = "Select * from postjob;";
														$query_run =  mysqli_query($conn,$query);
														
														if(mysqli_num_rows($query_run)>0)
														{
															$i=1;
															while($res=mysqli_fetch_array($query_run))
															{	
																print "<tr>";
																print "<td>".$res["CompanyName"]."</td>";
																print "<td>".$res["JobTitle"]."</td>";
																print "<td>".$res["Location"]."</td>";
																print "<td>".$res["Education"]."</td>";
																print "<td>".$res["Experience"]."</td>";
																print "<td>".$res["Salary"]."</td>";
																print "<td>".$res["Description"]."</td>";
																print "<td>".$res["StartDate"]."</td>";
																print "<td>".$res["EndDate"]."</td>";
																print "<td><button name='abc' class='btn-info btn-circle row-remove form-control'> <span class='glyphicon glyphicon-pencil'></span></button></td>";
																print "<td><button name='abc' class='btn-danger btn-circle row-remove form-control'> <span class='glyphicon glyphicon-trash'></span></button></td>";
																print "</tr>";
																$i++;
															}
															 //echo "Displayed";
														}else {
																echo "No results to display";
															}	
														
																mysqli_free_result($query_run);
																mysqli_close($conn);
														?>
								</table>
								<br><br>
							</form>
						</div>
				</div>
		</div>
	</div>
	<!--********************************************************************-->
	<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2016 Talent Yantra Assignment | By : Anusha Palekar
                </div>

            </div>
        </div>  
	</footer>
    <!-- Footer end-->
</body>
</html>
